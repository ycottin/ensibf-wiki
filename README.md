# ENSIBF² Club

Petit dépôt, grande mission aider les nouveaux arrivants avec des conseils pratiques et toutes les infos pour commencer la formation en toute sérénité.

## Utilisation

Pour installer et mettre à jour les dépendances les prérequis

```bash
pip install -r requirements.txt
```

Pour build le site

```bash
mkdocs build --clean --site-dir public
```

## Contributions

Ce wiki est ouvert aux contributions. N'hésitez à fork le projet puis à soumettre vos pull-requests pour le faire grandir et évoluer.

Les contributions suivantes sont très appréciées :

- Contributions locales (Ajout d'infos par ville)
- Correction en tout genre orthographe, markdown, etc
- Remplir les nombreux todo qui trainent

- Les contributions doivent respecter les [normes du markdown](https://github.com/DavidAnson/markdownlint/blob/v0.23.1/doc/Rules.md). Pour vous aider utilisé un linter markdown comme markdownlint sous vscode.
- Les commits et les pulls request sont à écrire en anglais même très basique
