# 🏘️ Logements

Voici quelques astuces pour trouver un logement.
Une majorité d'étudiants possèdent deux logements. Un sur Vannes pour l'école et un autre à proximité de leur lieu de travail.

Pour vous aider dans vos recherches de logement, le [site du bureau jeunesse de Vannes](http://www.bij-vannes.fr/ma-vie-a-vannes/le-logement/) propose plusieurs pistes.

## Appart-City 🏨

Envie de louer un appartement sur Vannes seulement pendant vos semaines de cours ? Eh oui c'est possible grâce à l'appart city. Pour environ **450€** en demandant la tarification étudiante pour une période scolaire. Vous pourrez ainsi bénéficier d'un logement à 5 minutes à pied des locaux l'ENSIBS.

Toutes les informations en détails [ici](https://www.appartcity.com/fr/offre-etudiante.html).

## Crous 🏠

La résidence universitaire [Lann Trussac](https://www.crous-rennes.fr/logement/lann-trussac/) se situe dans le quartier de Kercado, proche de l'IUT de Vannes (bus 1, 3 et 11). Elle comprend 145 studios et 150 chambres 9 m². La demande de logement doit être effectuée entre janvier et mai.
